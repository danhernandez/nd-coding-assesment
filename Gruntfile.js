module.exports = function(grunt) {
	grunt.initConfig({
		// import our package.JSON
		pkg: grunt.file.readJSON('package.json'),
		less: {
			// adding this here in case we need to run a production build later
      		dev: {
        		options: {
          			compress: true
        		},
        	files: {
          		"css/style.css": "less/style.less"
        		}
      		}
    	},
    	watch: {
    		styles: {
    			files: ['less/**/*.less'], // which files to watch
    			tasks: ['less'],
    			options: {
          		spawn: false,
          		reload: true
        		}
    		}
    	}
	});
	// Loads grunt serve
	grunt.loadNpmTasks('grunt-serve');
	// grunt watch for easier local dev
	grunt.loadNpmTasks('grunt-contrib-watch');
	// for pre-processing our CSS with less
	grunt.loadNpmTasks('grunt-contrib-less');
	// Watch any less changed files by default 
	grunt.registerTask('default', ['watch']);
}