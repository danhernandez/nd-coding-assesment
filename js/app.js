/*

Hello there, thanks or taking the time to have a look at this. 
I figured this would be the cleanest way of making this work across modern and legacy browsers with a focus on performance and simplicity, rather than on the latest and greatest framework (react, handlebars.js etc).

Nothing too crazy going on here, have just used standard AJAX calls and relied on just jQuery and moment.js [to help with the pesky task of parsing dates] as external libraries.

*/


$(function() {
'use strict'
	
	// Adding these here as global variables in case we need to change the method these generated in future, ie could be based on user input, dropdown etc.
	var username = 'twitter'
	var baseURL = 'https://api.github.com/users/'
	var fullURL	= baseURL+username+'/repos'
	// console.log('full url is: '+ fullURL);

	$.getJSON(fullURL,function(data){
      	// we've got the 'twitter' github repo call
		// hide the loader since we've loaded the content
		$('.loadingRepos').hide();

		var showRepos = 3;

		$(data).each(function(i,repos){
			// setting up vars for info we need to display
			var nameOfRepo = repos.name;
			var ownerAvatarUrl = repos.owner.avatar_url;
			var repoUrl = repos.html_url;
			var lastUpdated = repos.updated_at;
			var watchers = repos.watchers_count;
			var formattedDate = moment(lastUpdated).format('YYYY/MM/DD');
			// show only first 3 results
			if(i == showRepos) return false;
	      	// console.log(formattedDate);

	      	// have kept this in a single string variable in case this HTML ever needs to come from somehwere else, or updated easier in just the one place for future reference. 

	      	// Build out the boxes....
	      	var buildOutContent =
	      	'<div class="col-sm-4 col-md-4">'+
	      		'<div class="box">'+
	      			'<a title="'+nameOfRepo+'" href="'+repoUrl+'">'+
	      			'<img class="avatarUrl" src="'+ownerAvatarUrl+'" alt="OwnerUrl" /></a>'+
	      			'<a title="'+nameOfRepo+'" href="'+repoUrl+'"><h4>'+nameOfRepo+'</h4></a>'+
	      			'<p>Last Updated - <span class="date">'+formattedDate+'</span></p>'+
	      			'<p>Watchers - '+watchers+'</p>'+
	      		'</div>'+
	      	'</div>'

	      	$("#results").append(buildOutContent);
		});

	});

})();